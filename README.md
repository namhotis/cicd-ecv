# [ECV Digital] CICD - Exercice

## Description

This project is a simple-create-app. The goal is not to have a complex project, but learn to use Gitlab CICD.
The two team members haven't got the same environment and the projects are not sur same. Maxime uses Heroku, and Mathis uses his VMS with PM2 ans NGINX.

## Teams members

- Maxime Belbeoch
- Mathis Dyk-Bosseret

## What has been done

- A simple create react app project with some tests.
- Pipelines triggered at change: working test and deployment jobs

## Deployment URLs

- https://www.cicd-ecv.mathis-dyk.fr/
- https://guarded-bastion-70907.herokuapp.com/
